<?php
//single line
echo "single line comment";
echo "<br>";

//multiple line
echo "multiple line comment";
echo "<br>";

//shell style comment
;echo "shell style comment";
echo "<br>";

?>